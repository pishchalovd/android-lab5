package com.example.a1430169.lab5;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class Activity4 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_4);
    }

    public void return4(View view) {
        TextView tv4 = (TextView)findViewById(R.id.header4);
        String strReturn4 = tv4.getText().toString();
        Intent i = new Intent();
        i.putExtra("DATA3", strReturn4);
        setResult(RESULT_OK, i);
        finish();
    }
}
