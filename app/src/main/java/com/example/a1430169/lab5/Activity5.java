package com.example.a1430169.lab5;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class Activity5 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_5);
    }

    public void return5(View view) {
        TextView tv5 = (TextView)findViewById(R.id.header5);
        String strReturn5 = tv5.getText().toString();
        Intent i = new Intent();
        i.putExtra("DATA3", strReturn5);
        setResult(RESULT_OK, i);
        finish();
    }
}
