package com.example.a1430169.lab5;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class Activity2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);

        String value = getIntent().getExtras().getString("DATA1");

    }

    public void return1(View view) {
        String strReturn;
        if(view.getId() == R.id.button1) {
            strReturn = "Activity 2: One, 1, uno, un/une";
        }
        else
        {
            strReturn = "Activity 2: Two, 2 , due, deux";
        }

            Intent i = new Intent();
            i.putExtra("DATA1", strReturn);
            setResult(RESULT_OK, i);
            finish();
    }



}
