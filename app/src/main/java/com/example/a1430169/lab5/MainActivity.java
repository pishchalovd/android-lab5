package com.example.a1430169.lab5;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

    private TextView tv1;
    private String TAG= "INTDATA1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
       tv1 = (TextView)findViewById(R.id.result);

    }

    public void start2(View view)
    {

        Log.d(TAG, tv1.getText().toString());
        Intent i = new Intent(this, Activity2.class);
        i.putExtra("DATA1", tv1.getText().toString());
        // request code = 0, only one activity used
        startActivityForResult(i, 0);
    }

    public void start3(View view)
    {

        Log.d(TAG, tv1.getText().toString());
        Intent i = new Intent(this, Activity3.class);
        i.putExtra("DATA3", tv1.getText().toString());
        // request code = 0, only one activity used
        startActivityForResult(i, 0);
    }

    public void start4(View view)
    {
        String url = "http://www.google.com";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }


    public void start5(View view)
    {

        Intent i = new Intent(Intent.ACTION_VIEW,
                Uri.parse("geo:0,0?q=Fastiv"));
        startActivity(i);
    }




    protected void onActivityResult(int request, int result, Intent i) {
        String data;
        switch (result) {
            case RESULT_OK:
                Log.d(TAG, "result ok");
                if (i != null && i.hasExtra("DATA1")) {
                    data = i.getExtras().getString("DATA1");
                    Log.d("DATA1", data);
                    if (data == null)
                        tv1.setText("no data");
                    else
                        tv1.setText(data);
                }
                else if (i != null && i.hasExtra("DATA3")) {
                    data = i.getExtras().getString("DATA3");
                    Log.d("DATA3", data);
                    if (data == null)
                        tv1.setText("no data");
                    else
                        tv1.setText(data);
                }
                else {
                    tv1.setText("No intent or no extras");
                }
               break;
            case RESULT_CANCELED:
            default:
                Log.d(TAG, "result canceled");
                tv1.setText("Cancel returned from child Activity");
        }
    }
}
