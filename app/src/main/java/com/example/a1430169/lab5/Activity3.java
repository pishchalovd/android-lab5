package com.example.a1430169.lab5;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;



public class Activity3 extends AppCompatActivity {

    private String TAG= "INTDATA3";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_3);
    }

    public void start4(View view)
    {

        Log.d(TAG, "call activity 4");
        Intent i = new Intent(this, Activity4.class);
        // request code = 0, only one activity used
        startActivityForResult(i, 0);
    }

    public void start5(View view)
    {

        Log.d(TAG, "call activity 5");
        Intent i = new Intent(this, Activity5.class);
        // request code = 0, only one activity used
        startActivityForResult(i, 0);
    }

    protected void onActivityResult(int request, int result, Intent i) {
        String data;
        data = i.getExtras().getString("DATA3");
        Log.d("DATA3", data);
        switch (result) {
            case RESULT_OK:
                Log.d(TAG, "result ok");
                if (i != null && i.hasExtra("DATA3")) {
                    data = i.getExtras().getString("DATA3");
                    Log.d("DATA3", data);
                    i.putExtra("DATA3", data);
                    setResult(RESULT_OK, i);
                }
                break;
            case RESULT_CANCELED:
            default:
                Log.d(TAG, "result canceled or null");
        }
        finish();
    }



}
